# -*- mode: snippet -*-
# name: powershell-cmdlet
# uuid: powershell-cmdlet
# condition: t
# contributor: 9to5IT (https://gist.github.com/9to5IT/9620683)
# --
#requires -version 4

<#
.SYNOPSIS
  ${1:script-name} - ${2:overview of script}

.DESCRIPTION
  ${4:Brief description of script}

.PARAMETER ${5:Parameter_Name}
  ${6:Brief description of parameter input required. Repeat this attribute if required}

.INPUTS
  ${7:Inputs if any, otherwise state None}

.OUTPUTS Log File
  The script log file stored in C:\Windows\Temp\${1}.log

.NOTES
  Version:        ${8:1.0}
  Author:         ${9:Name}
  Creation Date:  ${10:Date}
  Purpose/Change: ${11:Initial version}

.EXAMPLE
  ${12:Example explanation goes here}

  ${13:Example goes here. Repeat this attribute for more than one example}
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  ${14:#Script parameters go here}
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins
Import-Module PSLogging

#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Script Version
$sScriptVersion = '{$8}'

#Log File Info
$sLogPath = ${15:'C:\Windows\Temp'}
$sLogName = ${16:'{$8}.log'}
$sLogFile = Join-Path -Path $sLogPath -ChildPath $sLogName

#-----------------------------------------------------------[Functions]------------------------------------------------------------

<#

Function ${17:FunctionName} {
  Param ()

  Begin {
    Write-LogInfo -LogPath $sLogFile -Message ${18:'description of what is going on>...'}
  }

  Process {
    Try {
      ${19:code goes here}
    }

    Catch {
      Write-LogError -LogPath $sLogFile -Message $_.Exception -ExitGracefully
      Break
    }
  }

  End {
    If ($?) {
      Write-LogInfo -LogPath $sLogFile -Message 'Completed Successfully.'
      Write-LogInfo -LogPath $sLogFile -Message ' '
    }
  }
}

#>

#-----------------------------------------------------------[Execution]------------------------------------------------------------

Start-Log -LogPath $sLogPath -LogName $sLogName -ScriptVersion $sScriptVersion
${20:#Script Execution goes here}
Stop-Log -LogPath $sLogFile