;;; ~/.doom.d/+bindings.el -*- lexical-binding: t; -*-

;;; * initiate

(map!
     (:after projectile
       :leader :prefix ("1" . "propositum")
       :desc "New propositum project" :g "n" #'+propositum/new-project
       :desc "New project from repo" :g "g" #'+propositum/new-project-from-repo
       :desc "Open in native file mgr" :g "e" #'+propositum/browse-file-directory
       ))

;;; * capture

(map!
 (:after org
   ; I use org-capture more than the scratch buffer - swap the keys around
   :leader
   :desc "org capture" :g "x" #'counsel-projectile-org-capture ; shows both project-specific & generic options
   :desc "pop scratch buffer" :g "X" #'doom/open-scratch-buffer
   ))

;;; * edit
;;; ** disable `s-x' on macOS to prevent accidental deletions
(map! "s-x" nil)

;;; * find

;;; ** org-mode agenda
;; we need to sort out a few keybindings due to clashes between packages (e.g.
;; =evil-org-agenda-map= and =org-super-agenda=) as well as remap a few new keys
;; (e.g. =origami-mode= for folding =org-super-agenda= headings.)

(map!
 (:after org-ql-view
   :map org-ql-view-map
   :desc "Save current org-ql view" :g "C-x C-s" #'org-ql-view-save
   :desc "Refresh org-ql view" :m "r" (lambda () (interactive) (org-ql-view-refresh) (message "org-ql: view refreshed")) ; bind org-ql refresh to org-agenda redo & show a message indicating refresh has taken place
    ))

(map!
 (:after org-super-agenda
   :map org-super-agenda-header-map
   :g "<tab>" #'origami-toggle-node ; enable folding for org-super-agenda-mode headings
   ))

(map!
 (:after (org-agenda org-super-agenda)
   :map evil-org-agenda-mode-map
   :desc "peek logbook" :m "<backtab>" #'org-quick-peek-agenda-current-item-logbook
   :desc "get outlook diary" :m "go" #'ol-get/outlook-today
   :desc "refresh agenda & ol calendar" :m "gr" #'+propositum/org-agenda-refresh
   ))
