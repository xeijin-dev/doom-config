# -*- mode: snippet -*-
# condition: (equal default-directory propositum-projects-archive-directory)
# --
;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (buffer-read-only . t)
  (header-line-format . (#(" archived " 0 10 (face propositum-archived-header-line))))))