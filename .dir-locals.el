;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (project-main-org-file . "doom-config.org")
  (propertized-project-name
   #("doom-config" 0 11
     (face
      (:foreground "#d50065" :background "#ced3dc"))))
  (project-anon-face
   (:foreground "#d50065" :background "#ced3dc"))
  (project-fg-colour . "#ced3dc")
  (project-bg-colour . "#d50065")
  (projectile-project-name . "doom-config")))
