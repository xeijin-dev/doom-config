# -*- mode: snippet -*-
# condition: (projectile-project-root)
# expand-env: ((pn (+propositum/ensure-proj-name (+propositum/ensure-proj-path projectile-project-root) (or projectile-project-name 'prompt))) (pth (+propositum/ensure-proj-path projectile-project-root)))
# --
;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (projectile-project-name . `(format "%S" (+propositum/call-attrib-fn 'projectile-project-name pn pth))`)
  (project-bg-colour . `(format "%S" (+propositum/call-attrib-fn 'project-bg-colour pn pth))`)
  (project-fg-colour . `(format "%S" (+propositum/call-attrib-fn 'project-fg-colour pn pth))`)
  (project-anon-face . `(format "%S" (car (+propositum/call-attrib-fn 'project-anon-face pn pth)))`)
  (propertized-project-name . `(format "%S" (+propositum/call-attrib-fn 'propertized-project-name pn pth))`)
  (project-main-org-file . `(format "%S" (+propositum/call-attrib-fn 'project-main-org-file pn pth))`)))
