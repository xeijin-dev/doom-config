;; -*- no-byte-compile: t; -*-
;;; ~/.doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:fetcher github :repo "username/repo"))
;; (package! builtin-package :disable t)

(package! org-projectile)
(package! org-super-agenda)
(package! org-ql)
(package! powershell) ; for custom 'powershell' module
(package! lsp-pwsh :recipe (:host github :repo "kiennq/lsp-powershell")) ; for custom 'powershell' module
(package! ahk-mode)
(package! pepita) ; execute splunk queries from within emacs
(package! ob-splunk :recipe (:host github :repo "xeijin/ob-splunk" :branch "patch-1")) ; splunk integration for org-babel
(package! all-the-icons-ivy) ; add all-the-icons to common ivy file/buffer commands
(package! request)
(package! restclient)
(package! ob-restclient)
(package! company-restclient)
(package! uuidgen)
(package! outshine)
(package! origami)
(package! excorporate)
(package! ol-get :recipe (:host gitlab :repo "xeijin-dev/ol-get" :files (:defaults "*.py" "*.ps1")))
(package! virtualenvwrapper)
(package! ox-jira :recipe (:host github :repo "stig/ox-jira.el" :branch "trunk")) ; master branch changed to trunk
(package! org-jira)
(package! quick-peek)
(package! org-quick-peek :recipe (:host github :repo "xeijin/org-quick-peek"))
(package! graphviz-dot-mode)
(package! gitignore-templates)
(package! ob-mermaid)
(package! mermaid-mode)
;; (package! company-emoji) ;; seems to be breaking doom install process
