(setq user-full-name "Your Name")

(setq user-mail-address "name@example.com")

(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("melpa" . "http://melpa.org/packages/")))

; tell customize where to save customizations
(setq custom-file (concat doom-private-dir "+custom.el"))
(when (file-exists-p custom-file)
  (load-file custom-file))

(after! org
  (remove-hook 'after-save-hook #'+literate-recompile-maybe-h))

(defun xei/add-to-emacs-path (append-path &rest path)
  "add PATH to both emacs own `exec-path' and to the `PATH' inherited by emacs from the OS (aka `process-environment').
  APPEND-PATH should be non-nil if you want the added path to take priority over existing paths

  this does not modify the actual OS `PATH' just the two emacs internal variables which deal with paths:

  `exec-path' is used when executables are called from emacs directly
  `process-environment' is used when executables are called via the `shell'"

  (dolist (p path)
    (add-to-list 'exec-path p append-path))

  ;; update `process-environment' with whatever is in `exec-path' right now
  (setenv "PATH" (mapconcat #'identity exec-path path-separator))
  (message "exec-path and process-environment synchronised"))

(defun xei/call-process (command &rest args)
  "Execute COMMAND with ARGS synchronously.

Returns (STATUS . OUTPUT) when it is done, where STATUS is the returned error
code of the process and OUTPUT is its stdout output."
  (with-temp-buffer
    (cons (or (apply #'call-process command nil t nil args)
              -1)
          (string-trim (buffer-string)))))

(defmacro csetq (&rest pairs)
  (declare (debug t))
  "For each SYMBOL VALUE pair, calls either `custom-set' or `set-default'."
  (let (forms)
    (while pairs
      (let ((variable (pop pairs))
            (value (pop pairs)))
        (push `(funcall (or (get ',variable 'custom-set) 'set-default)
                        ',variable ,value)
              forms)))
    `(progn ,@(nreverse forms))))

;; Run server if:
;; - Our EUID is not 0,
;; - We are not logged in via SSH,
;; - It is not already running.
(unless (equal (user-real-uid) 0)
  (unless (getenv "SSH_CONNECTION")
    (use-package! server
      :init
      (setq server-use-tcp t
            server-port 8081
            server-auth-key ; 64 chars, saved in ~/.emacs.d/server/server.
            "nickh8oow3Aph5ahje1eek1aish3Ohthu4Paengae0iketohGhaemi2iek5ae4ee")
      :config
      (unless (eq (server-running-p) t) ; Run server if not t.
          (server-start)))))

(use-package! tramp
  :custom
  (tramp-default-method "ssh")    ; ssh is faster than scp and supports ports.
  (tramp-use-ssh-controlmaster-options nil) ; Don't override SSH config.
  (tramp-password-prompt-regexp   ; Add verification code support.
   (concat
    "^.*"
    (regexp-opt
     '("passphrase" "Passphrase"
       "password" "Password"
       "Verification code")
     t)
    ".*:\0? *"))
  (when IS-WINDOWS
    (setq tramp-default-method "plink")))

(load-theme 'doom-spacegrey t)

(defun xei/font-setup ()
  "correctly setup unicode fonts to render `all-the-icons' glyphs properly,
  also sets-up the default font & guards against font unavailable config errors."

  (message "setting up fonts ...")

  ; set default font
  (when (member "SF Mono" (font-family-list))
    (when IS-WINDOWS
      (set-face-attribute 'default nil :font "SF Mono 11"))
    (when IS-MAC ;; account for higher resolution on macbook pro 13
      (set-face-attribute 'default nil :font "SF Mono 14")))


  ; get `all-the-icons' fonts to render properly
  (let ((unicode-fonts (list
                        "Weather Icons"
                        "github-octicons"
                        "FontAwesome"
                        "all-the-icons"
                        "file-icons"
                        "Material Icons"
                        "SF Pro Display"
                        "Segoe UI Emoji" ;... Windows only? Doesn't seem to work anyway.
                        ;; "Quivira" ; disabled as overwriting some material icons
                        "Symbola"
                        "Noto Sans Symbols2"
                        "SF Mono")))
    (dolist (fnt unicode-fonts)
      (when (member fnt (font-family-list))
        (set-fontset-font t 'unicode fnt nil 'prepend))))

  (message "setting up fonts ... done"))

(defcustom after-server-ui-init-hook '(xei/font-setup)
  "hook that runs after an emacs `daemon' first attempts to create a frame
  this hook is executed as `advice'`:after' the following functions: `server-create-tty-frame' and `server-create-window-system-frame'"
  :type 'hook
  :group 'propositum)

(let ((run-hook-fn '(lambda (&rest n) ; temp function will receive arguments ('n') but they are not used, only interested in running the hook
                      (run-hooks 'after-server-ui-init-hook))))

  (advice-add 'server-create-window-system-frame :after run-hook-fn)
  (advice-add 'server-create-tty-frame :after run-hook-fn))

(unless (daemonp)
  (run-hooks 'after-server-ui-init-hook))

(add-hook 'doom-init-ui-hook #'doom-init-theme-h)
(remove-hook 'after-make-frame-functions #'doom-init-theme-h)

(setq frame-title-format "%b | λ")

(setq +doom-dashboard-name "λI:home")

(setq +doom-dashboard-banner-dir "~/../other")

(setq +doom-dashboard-banner-file "ai-splash-small.png")

;; remove footer & 'loaded' information
(setq +doom-dashboard-functions
      '(doom-dashboard-widget-banner
        doom-dashboard-widget-shortmenu))

(after! persp-mode (setq persp-emacsclient-init-frame-behaviour-override -1))

(setq projectile-git-submodule-command nil)

(use-package! company-emoji
  :after company
  :config
  (add-to-list 'company-backends 'company-emoji))

(defun xei/is-windows-virtualised ()
  "determine if the current instance of emacs is running on a virtualised instance of Windows
  Returns the name of the virtual machine provider if non-nil."

  ;; with help from (and thanks to) Henrik on doom-emacs discord

  (require 'seq)

  (let* ((props '("Manufacturer" "Model" "SystemFamily"))
         (args (string-join props ","))
         (hypervisors ; should be correct based on some google research, but only parallels and vmware actually tested so far...
          '("Citrix" "Hyper-V" "Parallels" "VirtualBox" "VMWare")))

    (cl-destructuring-bind (status . output)
        (xei/call-process "wmic" "computersystem" "get" args "/format:list")
      ;; TODO Handle errors by checking STATUS
      (dolist (line (split-string output "\n" t))
        (add-to-list 'process-environment (concat "Wmi" line) 'append))
      (let ((wmi-vars (list (getenv "WmiModel")
                            (getenv "WmiManufacturer")
                            (getenv "WmiSystemFamily"))))

        (fset 'is: (lambda (p) ; wrap string-match lambda in a more readable fn name for pcase
                     (car (seq-remove #'null
                                      (mapcar
                                       (lambda (hv)
                                         (when (string-match-p (regexp-quote hv) p) hv))
                                       hypervisors)))))
        (catch 'vm
          (dolist (prop wmi-vars)
            (pcase (is: prop) ; 'is prop equal to any of the following'
              ("Parallels"
               (throw 'vm 'parallels))
              ("Citrix"
               (throw 'vm 'citrix))
              ("Hyper-V"
               (throw 'vm 'hyper-v))
              ("VirtualBox"
               (throw 'vm 'virtualbox))
              ("VMWare"
               (throw 'vm 'vmware))
              ('nil (throw 'vm nil)))))))))
